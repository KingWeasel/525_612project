----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/26/2019 01:54:34 PM
-- Design Name: 
-- Module Name: vga_tester - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga_tester is
Port (
        CLK100MHZ : in STD_LOGIC;
        BTND : in STD_LOGIC;
        SW : in std_logic_vector (15 downto 0);
        BTNL: in std_logic;
        BTNC : in std_logic;
        BTNR : in std_logic;
        BTNU : in std_logic;
        VGA_R : out STD_LOGIC_VECTOR (3 downto 0);
        VGA_G : out STD_LOGIC_VECTOR (3 downto 0);
        VGA_B : out STD_LOGIC_VECTOR (3 downto 0);
        VGA_HS : out std_logic;
        VGA_VS : out std_logic;
        LED : out std_logic_vector (15 downto 0)
      );
end vga_tester;

architecture Behavioral of vga_tester is
    signal color_in : std_logic_vector(31 downto 0);
    signal addr : unsigned(31 downto 0);
begin

    vga_controller : entity vga_ctl port map
    (
        clk => CLK100MHZ,
        reset => BTND,
        write_addr => addr,
        write_data => color_in,
        write_enable => BTNU,
        vga_r_out => VGA_R,
        vga_g_out => VGA_G,
        vga_b_out => VGA_B,
        vga_hs_out => VGA_HS,
        vga_vs_out => VGA_VS,
        leds => LED    
    );
    
    addr <= (15 => SW(15), 14 => SW(14), 13 => SW(13), 12 => SW(12), 
             11 => SW(11), 10 => SW(10), 9 => SW(9), 8 => SW(8),
             7 => SW(7),6 => SW(6), 5 => SW(5), 4 => SW(4),
             3 => SW(3), 2 => SW(2), 1 => SW(1), 0 => SW(0),
             others => '0');
    
    color_in(31 downto 12) <= (others => '0');
    color_in(11 downto 8) <= (others => BTNL);
    color_in(7 downto 4) <= (others => BTNC);
    color_in(3 downto 0) <= (others => BTNR);

end Behavioral;
