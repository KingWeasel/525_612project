library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.all;

entity vga_mem is
    Port 
    ( 
        clk : in STD_LOGIC;
        we : in STD_LOGIC;
        en : in STD_LOGIC;
        read_addr : in unsigned (15 downto 0);
        write_addr : in unsigned (15 downto 0);
        di : in STD_LOGIC_VECTOR (15 downto 0);
        do : out STD_LOGIC_VECTOR (15 downto 0);
        leds : out std_logic_vector (15 downto 0)
    );
end vga_mem;

architecture Behavioral of vga_mem is
    -- only maps to BRAM if 1D :(
    type block_memory is array (0 to 65535) 
        of std_logic_vector(15 downto 0);
    signal frame_buffer : block_memory;
begin
    
    leds <= std_logic_vector(read_addr);
    
    do_mem : process(clk)
    begin
        if(rising_edge(clk)) then
            if(en = '1') then
                do <= frame_buffer( to_integer(read_addr) );
                if(we = '1') then
                    frame_buffer( to_integer(write_addr) ) <= di;
--                    do <= di; 
                end if;
            end if;
        end if;
    end process do_mem;
    
end Behavioral;
