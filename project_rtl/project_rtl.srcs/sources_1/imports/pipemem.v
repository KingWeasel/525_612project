/************************************************
  The Verilog HDL code example is from the book
  Computer Principles and Design in Verilog HDL
  by Yamin Li, published by A JOHN WILEY & SONS
************************************************/
`include "mfp_ahb_const.vh"
module pipemem 
(
    we, addr,datain,clk,clrn, dataout,
    dbg_dmem_ce, dbg_dmem_we,dbg_dmem_din,dbg_dmem_addr,IO_Switch,
    IO_PB,IO_LED,IO_7SEGEN_N,IO_7SEG_N,IO_BUZZ, 
    IO_SPI_SDO, IO_SPI_RS, IO_SPI_SCK,UART_RX,
    IO_VGA_R, IO_VGA_G, IO_VGA_B, IO_VGA_HS, IO_VGA_VS
);          // MEM stage
    
    input         clk;                                // clock
    input         clrn;                               // reset
    input  [31:0] addr;                               // address
    input  [31:0] datain;                             // data in (to mem)
    input         we;                                 // memory write
    output [31:0] dataout;                            // data out (from mem)
    input  dbg_dmem_ce;
    input  dbg_dmem_we;
    input  [31:0] dbg_dmem_din;
    input  [31:0] dbg_dmem_addr;
    input  [`MFP_N_SW-1 :0] IO_Switch;
    input  [`MFP_N_PB-1 :0] IO_PB;
    output [`MFP_N_LED-1:0] IO_LED;
    output [ 7          :0] IO_7SEGEN_N;
    output [ 6          :0] IO_7SEG_N;
    output                  IO_BUZZ;                  
    output                  IO_SPI_SDO;
    output                  IO_SPI_RS;
    output                  IO_SPI_SCK;
    input                   UART_RX;
    output [ 3          :0] IO_VGA_R;
    output [ 3          :0] IO_VGA_G;
    output [ 3          :0] IO_VGA_B;
    output                  IO_VGA_HS;
    output                  IO_VGA_VS;
    
    wire[31:0] data_mem; //data driven by data memory
    wire[31:0] data_gpio; //data driven by GPIO module
    wire[31:0] dataBus; //Resulting bus from the currently selected peripheral
    
    assign dataout = dataBus;
    
 // write data memory // Check if memory mapped I/O
    wire[3:0] HSEL;
    parameter DMEM_FILE = "";


    pipelinedcpu_decode pipelinedcpu_decode(addr,HSEL);
    
    wire effectiveDMemWE = dbg_dmem_ce ? dbg_dmem_we : we;
    wire effectiveDMemCE = dbg_dmem_ce | HSEL[1];
    wire[31:0] effectiveDMemAddr = dbg_dmem_ce ? dbg_dmem_addr : addr;
    uram #(.A_WIDTH(12), .INIT_FILE(DMEM_FILE), .READ_DELAY(0)) dmem
        (.clk(clk), .we(effectiveDMemWE), .cs(effectiveDMemCE), .addr(effectiveDMemAddr), .data_in(dataBus), .data_out(data_mem));     // data memory
    
    pipelinecpugpio gpio 
    (
        .clk(clk),
        .clrn(clrn),
        .dataout(data_gpio),
        .datain(dataBus),
        .haddr(addr),
        .we(we),
        .HSEL(HSEL[3:2]),
        .IO_Switch(IO_Switch),
        .IO_PB(IO_PB),
        .IO_LED(IO_LED),
        .IO_7SEGEN_N(IO_7SEGEN_N),
        .IO_7SEG_N(IO_7SEG_N), 
        .IO_BUZZ(IO_BUZZ),
        .IO_SPI_SDO(IO_SPI_SDO),
        .IO_SPI_RS(IO_SPI_RS),
        .IO_SPI_SCK(IO_SPI_SCK),
        .IO_VGA_R(IO_VGA_R),
        .IO_VGA_G(IO_VGA_G),
        .IO_VGA_B(IO_VGA_B),
        .IO_VGA_HS(IO_VGA_HS),
        .IO_VGA_VS(IO_VGA_VS)
    );
    
    assign dataBus = dbg_dmem_we ? dbg_dmem_din :
                         we ? datain : //data driven by cpu
                    HSEL[1] ? data_mem :
                    HSEL[2] ? data_gpio :
                    32'b0;
    
endmodule
