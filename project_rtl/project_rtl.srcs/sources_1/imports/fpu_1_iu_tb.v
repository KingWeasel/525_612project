/************************************************
  The Verilog HDL code example is from the book
  Computer Principles and Design in Verilog HDL
  by Yamin Li, published by A JOHN WILEY & SONS
************************************************/
`timescale 1ns/1ns
`include "mfp_ahb_const.vh"
module fpu_1_iu_tb;
    reg         clk,memclk,clrn;
    wire [31:0] pc,inst,ealu,malu,walu;
    wire [31:0] e3d,wd;
    wire [4:0]  e1n,e2n,e3n,wn;
    wire        ww,stl_lw,stl_fp,stl_lwc1,stl_swc1,stl;
    wire        e;
    wire [4:0]  cnt_div,cnt_sqrt;
    reg [`MFP_N_SW-1  :0] IO_Switch;
    wire [          4  :0] IO_PB;
     wire [`MFP_N_LED-1 :0] IO_LED;
     wire [ 7           :0] IO_7SEGEN_N;
     wire [ 6           :0] IO_7SEG_N; 
     wire                   IO_BUZZ;
     wire                   IO_SPI_SDO;
     wire                   IO_SPI_RS;
     wire                   IO_SPI_SCK;
 
     reg                    UART_RX;

   fpu_1_iu cpu (.SI_ClkIn(clk),.memclk(memclk),
                 .SI_Reset_N(clrn),
                 .pc(pc),
                 .inst(inst),
                 .ealu(ealu),
                 .malu(malu),
                 .walu(walu),
                 .wn(wn),
                 .wd(wd),
                 .ww(ww),
                 .stl(stl),
                 .stl_fp(stl_fp),
                 .stl_lwc1(stl_lwc1),
                 .stl_swc1(stl_swc1),
                 .cnt_div(cnt_div),
                 .cnt_sqrt(cnt_sqrt),
                 .e1n(e1n),
                 .e2n(e2n),
                 .e3n(e3n),
                 .e3d(e3d),
                 .e(e),
                 .IO_Switch(IO_Switch),
                 .IO_PB(IO_PB),
                 .IO_LED(IO_LED),
                 .IO_7SEGEN_N(IO_7SEGEN_N),
                 .IO_7SEG_N(IO_7SEG_N), 
                 .IO_BUZZ(IO_BUZZ),
                 .IO_SPI_SDO(IO_SPI_SDO),
                 .IO_SPI_RS(IO_SPI_RS),
                 .IO_SPI_SCK(IO_SPI_SCK),
                 .UART_RX(UART_TXD_IN)); 

 
 //   fpu_1_iu cpu (clk,memclk,clrn,pc,inst,ealu,malu,walu,wn,
 //                 wd,ww,stl_lw,stl_fp,stl_lwc1,stl_swc1,
 //                 stl,cnt_div,cnt_sqrt,e1n,e2n,e3n,e3d,e);
    initial begin
           clrn   = 0;
           memclk = 0;
           clk    = 1;
        #1 clrn   = 1;
    end
    always #25 memclk = !memclk;
    always #50 clk  = !clk;
endmodule
/*
  24 -  52.001 ns
  48 -  76.001 ns
 128 - 412.000 ns
*/
