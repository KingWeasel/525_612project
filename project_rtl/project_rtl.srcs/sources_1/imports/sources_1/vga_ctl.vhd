----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- A general purpose VGA controller (64x64 pixels right now)
-- May 2019
--
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;
use IEEE.NUMERIC_STD.ALL;

entity vga_ctl is
    Port (  clk : in STD_LOGIC; --Clock
            reset : in std_logic; --async reset
            
            -- interface to VGA memory
            write_addr : in unsigned (31 downto 0);
            write_data : in std_logic_vector (31 downto 0);
            write_enable: in std_logic;
            
            -- VGA outputs
            vga_r_out: out std_logic_vector (3 downto 0);
            vga_g_out : out std_logic_vector (3 downto 0);
            vga_b_out : out std_logic_vector (3 downto 0);
            vga_hs_out : out std_logic;
            vga_vs_out : out std_logic;
            
            leds : out std_logic_vector (15 downto 0)     
          );
end vga_ctl;

architecture Behavioral of vga_ctl is

    -- display
    signal pulse_25MHz : std_logic; -- pixel clock
    signal horz_cntr   : unsigned (9 downto 0); -- horizontal scan position counter
    signal vert_cntr   : unsigned (9 downto 0); -- vertical scan position counter
    
    -- memory related
    signal mem_enable   : std_logic;
    signal read_enable  : std_logic; -- allows vga_ctl to shift out data from mem
    signal addr         : unsigned (15 downto 0);
    signal read_addr    : unsigned (15 downto 0);
    signal vga_out      : std_logic_vector (15 downto 0);
       
begin
    
    vga_buffer : entity vga_mem port map
    (
        clk => clk,
        we => write_enable,
        en => mem_enable,
        read_addr => read_addr,
        write_addr => write_addr(15 downto 0),
        di => write_data (15 downto 0),
        do => vga_out,
        leds => leds
    );
    
    pulse_gen_25MHz : entity pulse_gen port map -- generate pixel clock
    (   clk => clk,
        reset => reset,
        count_to => to_unsigned(3,27), -- 3 or about 25us (25MHz)
        pulse_10ns => pulse_25MHz
    );
    
    mem_enable <= write_enable or read_enable; -- allows either CPU or VGA access
    
    whole_screen : process(clk, reset)
    begin         
        
        if(reset = '1') then -- reset
            horz_cntr <= (others => '0');
            vert_cntr <= (others => '0');
            read_addr <= (others => '0');
        elsif(rising_edge(clk)) then
            if(pulse_25MHz = '1') then    
                
                -- count the screen
                if(horz_cntr = to_unsigned(799,10)) then --if whole line drawn, then EOL basically
                    horz_cntr <= (others => '0');
                    --vert increments when horizontal resets
                    if(vert_cntr = to_unsigned(520,10)) then --if whole column drawn
                        vert_cntr <= (others => '0');
                    else
                        vert_cntr <= vert_cntr + 1;
                        
                    end if;
                else
                    horz_cntr <= horz_cntr + 1;          
                end if; 
            
--                set Hsync and Vsync
                if((horz_cntr >= 656) and (horz_cntr < 752)) then
                    vga_hs_out <= '0';
                else
                    vga_hs_out <= '1';
                end if;
        
                if((vert_cntr >= 490) and (vert_cntr < 492)) then
                    vga_vs_out <= '0';
                else
                    vga_vs_out <= '1';
                end if;
  
--                set colors
                if((horz_cntr < 256) and (vert_cntr < 256)) then
                
                    if(read_addr = to_unsigned(65535, 16)) then
                        read_addr <= (others => '0');
                    else
                        read_addr <= read_addr + 1;
                    end if;
                    
                    read_enable <= '1';
                    vga_r_out <= vga_out(11 downto 8);
                    vga_g_out <= vga_out(7 downto 4);
                    vga_b_out <= vga_out(3 downto 0);
                else
                    read_enable <= '0';
                    vga_r_out <= (others => '0');
                    vga_g_out <= (others => '0');
                    vga_b_out <= (others => '0'); 
                end if;
            end if;
        end if;
    end process whole_screen;
    
end Behavioral;
