----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--module that outputs true when it detects
--an input value of true and a previous input value of false 
--------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity press_detector is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           btn_in : in STD_LOGIC;
           press_detected : out STD_LOGIC);
end press_detector;

architecture Behavioral of press_detector is
    signal btn_prev : std_logic;
begin
    
    process(clk, reset)--Dff
    begin
        if(reset = '1') then
            btn_prev <= '0';
        elsif(rising_edge(clk)) then
            btn_prev <= btn_in;
        end if;
    end process;
    
    press_detected <= btn_in and not btn_prev;-- curr input is 1 and priv input is 0

end Behavioral;
