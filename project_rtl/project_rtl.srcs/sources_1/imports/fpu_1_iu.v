/************************************************
  The Verilog HDL code example is from the book
  Computer Principles and Design in Verilog HDL
  by Yamin Li, published by A JOHN WILEY & SONS
************************************************/
// pipelined cpu with fpu, instruction memory, and data memory
`include "mfp_ahb_const.vh"
module fpu_1_iu 
(
    SI_ClkIn, memclk, SI_Reset_N,
    pc, inst,
    ealu, malu, walu,
    wn, wd, ww,
    stl_lw, stl_fp,stl_lwc1, stl_swc1, stl, 
    cnt_div, cnt_sqrt,
    e1n, e2n, e3n, e3d, e,
    IO_Switch, IO_PB,IO_LED,
    IO_7SEGEN_N,IO_7SEG_N,
    IO_BUZZ, 
    IO_SPI_SDO, IO_SPI_RS, IO_SPI_SCK,
    UART_RX,
    JB,
    IO_VGA_R, IO_VGA_G, IO_VGA_B, 
    IO_VGA_HS, IO_VGA_VS
); 
    
    input         SI_ClkIn, memclk, SI_Reset_N;               // clocks and reset
    output [31:0] pc, inst, ealu, malu, walu;
    output [31:0] e3d, wd;
    output [4 :0] e1n, e2n, e3n, wn;
    output        ww, stl_lw, stl_fp, stl_lwc1, stl_swc1, stl;
    output        e;                // for multithreading CPU, not used here
    output [ 4          :0] cnt_div, cnt_sqrt;     // for testing
    input  [`MFP_N_SW-1 :0] IO_Switch;
    input  [`MFP_N_PB-1 :0] IO_PB;
    output [`MFP_N_LED-1:0] IO_LED;
    output [ 7          :0] IO_7SEGEN_N;
    output [ 6          :0] IO_7SEG_N;
    output                  IO_BUZZ;                  
    output                  IO_SPI_SDO;
    output                  IO_SPI_RS;
    output                  IO_SPI_SCK;
    input                   UART_RX;
    inout  [ 8          :1] JB;
    output [ 3          :0] IO_VGA_R;   //JK add
    output [ 3          :0] IO_VGA_G;
    output [ 3          :0] IO_VGA_B;
    output                  IO_VGA_HS;
    output                  IO_VGA_VS;
    
    wire   [31:0] qfa, qfb, fa, fb, dfa, dfb, mmo, wmo;   // for iu
    wire   [4 :0] fs, ft, fd, wrn; 
    wire   [2 :0] fc;
    wire   [1 :0] e1c, e2c, e3c;                     // for fpu
    wire          fwdla, fwdlb, fwdfa, fwdfb, 
                  wf, fasmds, e1w, e2w, e3w, wwfpr;
    
    iu i_u 
    (
        e1n, e2n, e3n, e1w, e2w, e3w, 
        stl, 1'b0, dfb, e3d, SI_ClkIn, memclk, SI_Reset_N,
        fs, ft, wmo, wrn, wwfpr, mmo, fwdla, fwdlb, fwdfa, fwdfb, fd, fc, wf, fasmds,
        pc, inst, ealu, malu, walu, stl_lw, stl_fp, stl_lwc1, stl_swc1, 
        IO_Switch, IO_PB, IO_LED, IO_7SEGEN_N, IO_7SEG_N, IO_BUZZ, 
        IO_SPI_SDO, IO_SPI_RS, IO_SPI_SCK, UART_RX,JB,
        IO_VGA_R, IO_VGA_G, IO_VGA_B, IO_VGA_HS, IO_VGA_VS
    );      // for testing
    
    regfile2w fpr 
    (
        fs, ft, wd, wn, ww, wmo, wrn, wwfpr,
        ~SI_ClkIn, SI_Reset_N, qfa, qfb
    );
    
    mux2x32 fwd_f_load_a (qfa, mmo, fwdla, fa);       // forward lwc1 to fp a
    mux2x32 fwd_f_load_b (qfb, mmo, fwdlb, fb);       // forward lwc1 to fp b
    mux2x32 fwd_f_res_a  (fa, e3d, fwdfa, dfa);       // forward fp res to fp a
    mux2x32 fwd_f_res_b  (fb, e3d, fwdfb, dfb);       // forward fp res to fp b
    
    fpu fp_unit 
    (
        dfa, dfb, fc, wf, fd, 1'b1,
        SI_ClkIn, SI_Reset_N, e3d, wd, wn, ww,
        stl, e1n, e1w, e2n, e2w, e3n, e3w,
        e1c, e2c, e3c, cnt_div, cnt_sqrt, e, 1'b1
    );
    
endmodule
