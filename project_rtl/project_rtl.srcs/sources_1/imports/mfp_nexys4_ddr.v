// mfp_nexys4_ddr.v
// January 1, 2017
// Modified by N Beser for Li Architecture 11/2/2017
//
// Instantiate the sccomp system and rename signals to
// match the GPIO, LEDs and switches on Digilent's (Xilinx)
// Nexys4 DDR board

// Outputs:
// 16 LEDs (IO_LED) 
// Inputs:
// 16 Slide switches (IO_Switch),
// 5 Pushbuttons (IO_PB): {BTNU, BTND, BTNL, BTNC, BTNR}
//

`include "mfp_ahb_const.vh"

module mfp_nexys4_ddr
( 
    input                   CLK100MHZ,
    input                   CPU_RESETN,
    input                   BTNU, BTND, BTNL, BTNC, BTNR, 
    input  [`MFP_N_SW-1 :0] SW,
    output [`MFP_N_LED-1:0] LED,
    inout  [ 8          :1] JB,
    output [ 7          :0] AN,
    output                  CA, CB, CC, CD, CE, CF, CG,
    output [ 1          :1] JC,
    output [ 4          :1] JD,
    input                   UART_TXD_IN,
    output [ 3          :0] VGA_R,      // Jacob Kunnappally added these 5
    output [ 3          :0] VGA_G,
    output [ 3          :0] VGA_B,
    output                  VGA_HS,
    output                  VGA_VS
);

  // Press btnCpuReset to reset the processor. 
        
    wire clk_out; 
  
    reg         SI_ClkIn, memclk, SI_Reset_N;
    wire [31:0] pc, inst, ealu, malu, walu;
    wire [31:0] e3d, wd;
    wire [4:0]  e1n, e2n, e3n, wn;
    wire        ww, stl_lw, stl_fp, stl_lwc1, stl_swc1, stl;
    wire        e;
    wire [4:0]  cnt_div, cnt_sqrt;
    
    fpu_1_iu cpu 
    (
        .SI_ClkIn(CLK100MHZ), .memclk(memclk), .SI_Reset_N(CPU_RESETN),
        .pc(pc), .inst(inst),
        .ealu(ealu), .malu(malu), .walu(walu),
        .wn(wn), .wd(wd), .ww(ww),
        .stl(stl), .stl_fp(stl_fp), .stl_lwc1(stl_lwc1), .stl_swc1(stl_swc1),
        .cnt_div(cnt_div), .cnt_sqrt(cnt_sqrt),
        .e1n(e1n), .e2n(e2n), .e3n(e3n), .e3d(e3d), .e(e), 
        .IO_Switch(SW), .IO_PB({BTNU, BTND, BTNL, BTNC, BTNR}),
        .IO_LED(LED), .IO_7SEGEN_N(AN), .IO_7SEG_N({CA,CB,CC,CD,CE,CF,CG}), 
        .IO_BUZZ(JC[1]),
        .IO_SPI_SDO(JD[3]), .IO_SPI_RS(JD[1]), .IO_SPI_SCK(JD[2]),
        .UART_RX(UART_TXD_IN),
        .JB(JB),
        .IO_VGA_R (VGA_R), .IO_VGA_G (VGA_G), .IO_VGA_B (VGA_B), // hooks out of fpu cpu for display
        .IO_VGA_HS (VGA_HS), .IO_VGA_VS (VGA_VS)
    ); 
        
endmodule
