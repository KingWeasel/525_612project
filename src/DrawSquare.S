.text                 # code segment
#.set noreorder        # code segment

# Note: <base address><vertical pix address><horizontal pix address>
  
main:
	lui  $s0, 0xbf80			# LED address
	lui  $s1, 0xbf90			# VGA address
	ori $s4, $s0, 0x4			# switches
	addi $t5, $0, 0				# place to form full VGA address
	addi $t8, $0, 0				# pixel counter
	addi $t4, $0, 0				# where to put switch value

load_frame: 
	lw $t4, 0($s4)				# get switch value
	andi $t4, $t4, 0xFFF		# mask to get just bottom 12 vals 
	or $t5, $s1, $t8			# full VGA pixel address
	andi $t8, $t8, 0xFFFF       # mask out upper bytes
	sw $t4, 0($t5)	    		# write to VGA
	addi $t8, $t8, 1
	sw $t8, 0($s0)
	j load_frame
	
