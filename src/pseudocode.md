remember nops after branches (branch delay slot)

main:
- load LED address
- load push button address
- load VGA base address

loop:
- scan push button values
- shift value by 1
- compare masked value to '1' to see if push button pressed
- if yes branch to update current button function

current button: 
- compare current button to last button
- if different update last button and fall into draw function, otherwise jump back to loop


draw:
- use last button to figure out order to scan registers
- if up, scan all sequentially incrementing
- if down, scan backwards
- if left, scan ends of rows, then decrement and 
- if right scan beginnings of rows and increment