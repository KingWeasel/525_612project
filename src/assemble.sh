#!/bin/sh
mips-mti-elf-as -mips1 -EB -g $1 &&
mips-mti-elf-objcopy -O binary -j .text a.out imem.bin &&
mips-mti-elf-objcopy -O binary -j .data a.out dmem.bin &&
mips-mti-elf-objdump -s -j .data &&
mips-mti-elf-objdump -d -j .text &&
xxd -c 4 -ps imem.bin > imem.hex &&
xxd -c 4 -ps dmem.bin > dmem.hex
cp imem.hex $2
