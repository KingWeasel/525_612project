.text                 # code segment
.set noreorder        # code segment
  
main:
	addi $13, $0, 2048 # rollover comparator
	lui $15, 0x0000
	srl $15, $15, 16
	lui $15, 0xFFFF
	lui $14, 0xbf90    # $14 = VGA 
	and $14, $14, $15

	addi $10, $0, 0xFFF # the color white

white_square:
	sw $10, 0($14)
	addi $14, $14, 1
	j white_square
