# EN 525.612 Computer Architecture Project Progress Report
**Jacob Kunnappally | 7 April 2019**

## Stated Goals for this Time Period
1. Procurement of parts
2. Functional test from manufacturer examples
3. Examination of driver library source code
4. Begin investigation of modification of existing hardware desgins to combine (USB, VGA, MIPS, etc.)
5. Preparation of status report

### Development Parts Procurement

I was able to procure both the VGA and USB Host daughterboards specified in the proposal. After some though, I decided the VGA daughterboard would be redundant, as I already have a VGA control described in VHDL from a previous course.

### Development Parts Functional Exercise

After setting aside the VGA daughterboard, I assembled and tested the USB Host daughterboard using given examples, an external USB keyboard, and a host computer for reading Arduino serial output.

### Examination of Example Libraries

After examining given libraries, I found that they are primarily for interacting with the daughterboard via SPI. The firmware running on the board itself that handles memory mapping and data transfer is what I'm really after.

This being the case, I intend to study I/O and memory mapping in the abstract using "bare metal" examples from the internet. I have found many so far that show how to use a Raspberry Pi in this context, such as this [ github library ](https://github.com/dwelch67/raspberrypi).

### Hardware Modification

I have yet to begin investigation of putting functional blocks together in order to form a complete system.

### Overall Project Status

I am continuing course on this effort. Once we get to the point in the class that there are no other assignments due, I will have much more time and thus a much better chance at making progress.