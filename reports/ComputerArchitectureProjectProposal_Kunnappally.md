# EN 525.612 Computer Architecture Project Proposal
**Jacob Kunnappally | 7 March 2019**

## Scope
The purpose of this project is to design and implement a system centered around a MIPS 32 CPU programmed onto a [ Digilent Nexsys 4 DDR FPGA development board ](#nexsys-4-ddr-board). The system will contain the following major components:

1. 1 Digilent Nexsys 4 DDR FPGA
2. 1 USB [ HID class ](#hid-specification) keyboard 
3. 1 Monitor with VGA input

The Nexsys board will take input from the keyboard via a USB controller and mapped memory. Based on the data received from the keyboard, the dev board will update a bitmap array in memory that governs the output of a VGA controller. The output of the onboard VGA port will be connected to the monitor, which will display the current state of the bitmap pictorially. Essentially, I intend to design and implement a standalone, volatile memory-only text editor running on a MIPS 32-based computer. Below is a high-level system diagram: 

<img src="pics/SystemDiagram.png" width=50% class="center" alt="System Diagram">

The Nexsys board will be powered by an external source via USB interface, the monitor will be powered by standard 120VAC hotel power, and the keyboard will be powered by the Nexsys board via USB interface.

## Relevance to Computer Architecture
This project is an exercise in memory-mapped IO. In it, I will have to interface the MIPS to a USB controller and VGA controller that will either write or read bits from areas of memory. I foresee also learning a great deal about the intricacies of how instruction and data memory interface to the rest of the CPU, as well as possibly learning about virtual memory and memory management. Potentially, I will have to deal with how to scale text for display.

This project highlights how human interface to a computer is implemented at the hardware/software interface. The fully realized system will necessarily have to use schemes that are the basic building blocks for device drivers and graphics processing.

## Resources
In addition to the major system components, a number of other resources are required to fully implement the solution. To program the FPGA hardware, a host computer will required. To load a program onto the implemented hardware, the [ Bus Blaster JTAG Debugger ](#bus-blaster-jtag-debugger) supplied by the class will be used in conjunction with the host computer. All code development, both hardware and firmware, will occur on the host computer.

In FPGA Design Using VHDL (525.642), the class was asked to implement a VGA controller that displayed a blue and green checkerboard with a movable red square. I should be able to generalize this code so that it reads from the data memory in the implemented MIPS. I also intend to take advantage of various USB IP from either the Xilinx libraries or [ OpenCores.org ](https://opencores.org/).

In addition to the parts already described, I will buy a [ VGA daughterboard ](#arduino-vga-shield) (shield) and [ USB host shield ](#arduino-usb-host-shield) for the Arduino UNOv3 microcontroller. As a platform with pre-built hardware and software libraries (often open source), I can learn the high level concepts associated with interfacing with a USB HID keyboard and VGA monitor before applying them to the more advanced MIPS-based system. Put more simply, I will examine firmware library sources to learn the rules of USB and VGA.

## Tentative Schedule
- 7 March - 31 March: Arduino Development 
   - Procurement of parts
   - Functional test from manufacturer examples
   - Examination of driver library source code
   - Begin investigation of modification of existing hardware designs to combine (USB, VGA, MIPS, etc.)
   - Preparation of status report
- 1 April - 30 April: MIPS/Nexsys implementation
   - Deliver status report
   - Implement development hardware on FPGA
   - Modify Arduino-developed code for MIPS 
   - Implement [ mandatory expectations ](#mandatory)
   - Possibly implement [ optional expectations ](#optional)
   - Begin preparation of final report and lecture (with demo)
- 1 May - 12 May: Presentation
  - Finish report and lecture and submit
  - Present lecture online

## Expectation of Results (Realizable within Time Frame)
### Mandatory
- Read keyboard output into MIPS memory and react to USB controller interrupts
- Keep display bitmap in memory, possibly implement some sort of hardware lock while bitmap write is occurring to avoid display blips
- Have the MIPS react to full [ ASCII character set ](#ascii-character-set), as well as direction arrow keys (this avoids having to make the editor modal)
- Implementation of full ASCII character set for display (backspace deletes character, enter moves cursor to next line, etc.)
- Status Report
- Final Report
### Optional
- Text scaling
- Programmable text color via external switches
- Save text to non-volatile memory via key combination (very optimistic)

## References
1. #### [ Arduino USB host shield ](  https://www.amazon.com/SenMod-Arduino-Realize-Function-Android/dp/B01JYNDNWA/ref=sr_1_3?keywords=arduino+usb+shield&qid=1551968984&s=gateway&sr=8-3 )

2. #### [ Arduino VGA shield ]( https://www.micro-nova.com/products/nv-1a )

3. #### [ ASCII character set ]( https://en.wikipedia.org/wiki/ASCII )

4. #### [ Bus Blaster JTAG Debugger ]( http://dangerousprototypes.com/docs/Bus_Blaster )

5. #### [ HID Specification ]( https://www.usb.org/sites/default/files/documents/hid1_11.pdf )

6. #### [ Nexsys 4 DDR board ]( https://store.digilentinc.com/nexys-4-ddr-artix-7-fpga-trainer-board-recommended-for-ece-curriculum/ )